/* Realizar un Programa Java que lea un número entero de 3 cifras y muestre por
separado las cifras del número.*/
package tarea1estructuras;
import javax.swing.JOptionPane;
public class pregunta10 {
public static void main(String[] args){
        String t=JOptionPane.showInputDialog("Escribe una cantidad en Bolivianos");
        int number = Integer.parseInt(t);
        if(countcifras(number)){
            JOptionPane.showMessageDialog(null, cifras(number));
        }else{
            JOptionPane.showMessageDialog(null, "error");
        }
    }
    public static String cifras (int number){
        return (number/100)+" - "+((number/10)%10)+" - " + (number%10);
    }
    public static boolean countcifras (int number){
         int count=0;
         for (int i=number;i>0;i/=10){
             count++;
         }
         if(count == 3){
             return true;
         }else{
             return false;
         }
    }
}
