/*Realizar un programa que nos convierta un número en base decimal a binario. Esto lo realizara un método al que le pasaremos el numero como parámetro, devolverá un String con el numero convertido a binario. Para convertir un numero
decimal a binario, debemos dividir entre 2 el numero y el resultado de esa división
se divide entre 2 de nuevo hasta que no se pueda dividir mas, el resto que
obtengamos de cada división formara el numero binario, de abajo a arriba.
Veamos un ejemplo: si introducimos un 8 nos deberá*/
package tarea1estructuras;
import javax.swing.JOptionPane;
public class pregunta6 {
  public static void main(String[] args) {
        String texto=JOptionPane.showInputDialog("Introduce un numero");
        int numero=Integer.parseInt(texto);
        String bin=decimalBinario(numero);
         JOptionPane.showMessageDialog(null,bin);
    }
   public static String decimalBinario (int numero){
        String bin="";
        String digito;
        for(int i=numero;i>0;i/=2){
            if(i%2==1){
                digito="1";
            }else{
                digito="0";
            }
            bin=digito+bin;
        }
        return bin;
    }
}

