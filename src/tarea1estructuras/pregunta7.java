/*Realizar un programa que cuente el número de cifras de un número 
entero positivo (hay que controlarlo) pedido por teclado. 
Crea un método que realice esta acción , pasando el numero por 
parametros, devolvera el numero de cifras-*/
package tarea1estructuras;
import javax.swing.JOptionPane;
public class pregunta7 {
 public static void main(String[] args) {
        int num=0;
        String texto=JOptionPane.showInputDialog("Introduce un numero");
        num=Integer.parseInt(texto);
        if(num>0){
            int numCifras=cuentaCifras(num);
            JOptionPane.showMessageDialog(null, numCifras);
        }
    }
    public static int cuentaCifras (int num){
         int count=0;
         for (int i=num;i>0;i/=10){
             count++;
         }
         return count;
    }
}
