/*Realizar un programa Java que calcule el área de un triángulo en función de las
longitudes de sus lados (a, b, c), según la siguiente fórmula: Area =
RaizCuadrada(p*(p-a)*(p-b)*(p-c)) donde p = (a+b+c)/2 Para calcular la raíz
cuadrada se utiliza el método Math.sqrt()*/
package tarea1estructuras;
import javax.swing.JOptionPane;
public class pregunta9 {
public static void main(String[] args) {
        double  a , b , c , p = 0;
        String texto=JOptionPane.showInputDialog("Introduzca longitud del  primero lado del triangulo : ");
        a=Integer.parseInt(texto);
        String texto2=JOptionPane.showInputDialog("Introduzca longitud del  segundo lado del triangulo : ");
        b=Integer.parseInt(texto2);
        String texto3=JOptionPane.showInputDialog("Introduzca longitud del  tercero lado del triangulo : ");
        c=Integer.parseInt(texto3);
             JOptionPane.showMessageDialog(null,area(a,b,c,p));
}
public static double area(double a , double b ,double c ,double p){ 
    p =(a+b+c)/2;
    return Math.sqrt(p*(p-a)*(p-b)*(p-c));
   }
}